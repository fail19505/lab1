## Національний технічний університет України<br>“Київський політехнічний інститут ім. Ігоря Сікорського”

## Факультет прикладної математики<br>Кафедра системного програмування і спеціалізованих комп’ютерних систем


# Лабораторна робота №1<br>"Базова робота з git"

## КВ-12 Гусельніков Антон

## Хід виконання роботи

### 1. Зклонувати будь-який невеликий проєкт open-source з github

```
$ git clone https://gitlab.com/redhat/centos-stream/rpms/java-21-openjdk
Cloning into 'java-21-openjdk'...
warning: redirecting to https://gitlab.com/redhat/centos-stream/rpms/java-21-openjdk.git/
remote: Enumerating objects: 42, done.
remote: Counting objects: 100% (38/38), done.
remote: Compressing objects: 100% (33/33), done.
remote: Total 42 (delta 5), reused 36 (delta 3), pack-reused 4
Unpacking objects: 100% (42/42), 139.94 KiB | 1.08 MiB/s, done.
```
## Часткове клонування
```
git clone https://gitlab.com/redhat/centos-stream/rpms/java-21-openjdk --depth=1 --single-branch --branch=c8s java-21-openjdk-shallowCloning into 'java-21-openjdk-shallow'...
warning: redirecting to https://gitlab.com/redhat/centos-stream/rpms/java-21-openjdk.git/
remote: Enumerating objects: 24, done.
remote: Counting objects: 100% (24/24), done.
remote: Compressing objects: 100% (23/23), done.
remote: Total 24 (delta 1), reused 24 (delta 1), pack-reused 0
Unpacking objects: 100% (24/24), 121.43 KiB | 7.59 MiB/s, done.
```
## Порівнюємо отримані репозиторії
```
$ du -sh ./*/.git
668K	./java-21-openjdk/.git
460K	./java-21-openjdk-shallow/.git
```

### 2. Зробити не менше трьох локальних комітів

## Модифікував файл java-21-openjdk.spec та створив новий 1.txt.

```
$ git status
On branch c9s
Your branch is up to date with 'origin/c9s'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   java-21-openjdk.spec

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	1.txt

no changes added to commit (use "git add" and/or "git commit -a")
```
Комітимо зміни файлу, який відслідковується гітом, у репозиторії: 
```
$ git commit -am "some changes to java-21-openjdk.spec"
[c9s b845988] some changes to java-21-openjdk.spec
 1 file changed, 1 insertion(+)
```

Результат коміту:
```
$ git status
On branch c9s
Your branch is ahead of 'origin/c9s' by 1 commit.
  (use "git push" to publish your local commits)

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	1.txt

nothing added to commit but untracked files present (use "git add" to track)
```
Додаємо та комітимо новий файл 1.txt:
```
$ git add 1.txt

$ git status
On branch c9s
Your branch is ahead of 'origin/c9s' by 1 commit.
  (use "git push" to publish your local commits)

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
	new file:   1.txt

$ git commit  -m "create 1.txt file"
[c9s 1eac977] create 1.txt file
 1 file changed, 1 insertion(+)
 create mode 100644 1.txt

$ git status
On branch c9s
Your branch is ahead of 'origin/c9s' by 2 commits.
  (use "git push" to publish your local commits)

nothing to commit, working tree clean
```

Багаторядкове повідомлення коміту:
```
$ git status
On branch c9s
Your branch is ahead of 'origin/c9s' by 2 commits.
  (use "git push" to publish your local commits)

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	2.txt
	3.txt

nothing added to commit but untracked files present (use "git add" to track)

$ git add 2.txt 3.txt

$ git status
On branch c9s
Your branch is ahead of 'origin/c9s' by 2 commits.
  (use "git push" to publish your local commits)

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
	new file:   2.txt
	new file:   3.txt

$ git commit -m "New files:
> 
> 1.txt
> 2.txt
> "
[c9s be3da88] New files:
 2 files changed, 2 insertions(+)
 create mode 100644 2.txt
 create mode 100644 3.txt

$ git status
On branch c9s
Your branch is ahead of 'origin/c9s' by 3 commits.
  (use "git push" to publish your local commits)

nothing to commit, working tree clean
```
### 3. Продемонструвати уміння вносити зміни до останнього коміту за допомогою опції --amend.

```
$ git status
On branch c9s
Your branch is ahead of 'origin/c9s' by 3 commits.
  (use "git push" to publish your local commits)

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	4.txt

nothing added to commit but untracked files present (use "git add" to track)

$ git add 4.txt

$ git commit --amend -m "create 2.txt, 3.txt and 4.txt"
[c9s bcceda8] create 2.txt, 3.txt and 4.txt
 Date: Sat Oct 7 22:56:18 2023 +0300
 3 files changed, 3 insertions(+)
 create mode 100644 2.txt
 create mode 100644 3.txt
 create mode 100644 4.txt

$ git status 
On branch c9s
Your branch is ahead of 'origin/c9s' by 3 commits.
  (use "git push" to publish your local commits)

nothing to commit, working tree clean
```
### 4. Продемонструвати уміння об'єднати кілька останніх комітів в один за допомогою git reset.

```
$ git reset HEAD~2

$ git status 
On branch c9s
Your branch is ahead of 'origin/c9s' by 1 commit.
  (use "git push" to publish your local commits)

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	1.txt
	2.txt
	3.txt
	4.txt

nothing added to commit but untracked files present (use "git add" to track)

$ git add 1.txt 2.txt 3.txt 4.txt

$ git commit -m "Create 1-4.txt files"
[c9s 283aba4] Create 1-4.txt files
 4 files changed, 4 insertions(+)
 create mode 100644 1.txt
 create mode 100644 2.txt
 create mode 100644 3.txt
 create mode 100644 4.txt

$ git status 
On branch c9s
Your branch is ahead of 'origin/c9s' by 2 commits.
  (use "git push" to publish your local commits)

nothing to commit, working tree clean
```

### 5. Видалити файл(и) одним способом на вибір.

```
$ rm 4.txt

$ git status
On branch c9s
Your branch is ahead of 'origin/c9s' by 2 commits.
  (use "git push" to publish your local commits)

Changes not staged for commit:
  (use "git add/rm <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	deleted:    4.txt

no changes added to commit (use "git add" and/or "git commit -a")

$ git add 4.txt

$ git status
On branch c9s
Your branch is ahead of 'origin/c9s' by 2 commits.
  (use "git push" to publish your local commits)

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
	deleted:    4.txt

$ git commit -m "delete 4.txt"
[c9s b26885f] delete 4.txt
 1 file changed, 1 deletion(-)
 delete mode 100644 4.txt
```

### 6. Перемістити файл(и) одним способом на вибір.

```
$ git mv 3.txt 3_1.txt

$ git status
On branch c9s
Your branch is ahead of 'origin/c9s' by 3 commits.
  (use "git push" to publish your local commits)

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
	renamed:    3.txt -> 3_1.txt

$ git commit -m "rename 3.txt to 3_1.txt"
[c9s fd91f83] rename 3.txt to 3_1.txt
 1 file changed, 0 insertions(+), 0 deletions(-)
 rename 3.txt => 3_1.txt (100%)
```

### 7. Гілкування

```
$  git checkout -b my-local-branch-1
Switched to a new branch 'my-local-branch-1'

$  git status
On branch my-local-branch-1
Untracked files:
  (use "git add <file>..." to include in what will be committed)
	l1.txt

nothing added to commit but untracked files present (use "git add" to track)

$ git add l1.txt

$ git commit -m "create l1.txt"
[my-local-branch-1 e83e84f] create l1.txt
 1 file changed, 1 insertion(+)
 create mode 100644 l1.txt

$ git checkout -b my-local-branch-2
Switched to a new branch 'my-local-branch-2'

$ git add l2.txt

$ git commit -m "create l2.txt"
[my-local-branch-2 950bfee] create l2.txt
 1 file changed, 1 insertion(+)
 create mode 100644 l2.txt

$ git checkout -b my-local-branch-3
Switched to a new branch 'my-local-branch-3'

$ git add l3.txt

$ git commit -m "create l3.txt"
[my-local-branch-3 d4a7ea0] create l3.txt
 1 file changed, 1 insertion(+)
 create mode 100644 l3.txt

$ git checkout c9s
Switched to branch 'c9s'
Your branch is ahead of 'origin/c9s' by 4 commits.
  (use "git push" to publish your local commits)
```

### 8.Продемонструвати уміння знайти в історії комітів набір комітів, в яких була зміна по
конкретному шаблону в конкретному файлі

```
$ git log -G '2_tmp' 2.txt
commit 42bcf18fec78d24c9e7cfa30b0de3008aac42958 (HEAD -> c9s)
Author: FAIL1950 <fail19505@gmail.com>
Date:   Sun Oct 8 01:44:19 2023 +0300

    2.txt modified

$ git show 42bcf18fec78d24c9e7cfa30b0de3008aac42958 2.txt
commit 42bcf18fec78d24c9e7cfa30b0de3008aac42958 (HEAD -> c9s)
Author: FAIL1950 <fail19505@gmail.com>
Date:   Sun Oct 8 01:44:19 2023 +0300

    2.txt modified

diff --git a/2.txt b/2.txt
index e7b4ad3..2c07176 100644
--- a/2.txt
+++ b/2.txt
@@ -1 +1 @@
-2.txt
+2_tmp.txt

```